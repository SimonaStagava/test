<?php

class Validation 
{
    private $data;
    private $empty = [];
    private $required = [
        'sku',
        'name',
        'price',
        'category',
        'height',
        'width',
        'lenght',
        'size',
        'weight'
      ];

    public function __construct($data = [])
    {
        $this->data = $data;

        foreach ($data as $key => $value) {
            $value = is_array($value) ? $value : trim($value);
            if (empty($value) && in_array($key, $this->required)) {
                $this->empty[] = $key;
                $$key = '';
            } 
        }
    }

    public function validation()
    {
        return $this->empty;
    }
}

?>