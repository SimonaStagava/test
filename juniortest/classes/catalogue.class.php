<?php

class Catalogue extends Db 
{
    public $data;
    public $delete = [];

    public function createDb() 
    {
        $create = $this->connection();
        return $create;
    }

    public function insert()
    {
        $sku = $_POST['sku'];
        $name = $_POST['name'];
        $price = $_POST['price'];
        $category = $_POST['category'];
        $size = !empty($_POST['size']) ? $_POST['size'] : 'NULL';
        $weight = !empty($_POST['weight']) ? $_POST['weight'] : 'NULL';
        $height = !empty($_POST['height']) ? $_POST['height'] : 'NULL';
        $width = !empty($_POST['width']) ? $_POST['width'] : 'NULL';
        $lenght = !empty($_POST['lenght']) ? $_POST['lenght'] : 'NULL';

        $query = "INSERT INTO products (sku, name, price, category, size, weight, 
                        height, width, lenght) VALUES (:sku, :name, :price, :category, :size, 
                        :weight, :height, :width, :lenght)";

        $stmt = $this->connection()->prepare($query);

        $exec = $stmt->execute(array(':sku'=>$sku, ':name'=>$name, ':price'=>$price,
                ':category'=>$category, ':size'=>$size, ':weight'=>$weight,
                ':height'=>$height, ':width'=>$width, ':lenght'=>$lenght));
    }

    public function delete()
    {
        $array = $_POST['data'];

        foreach ($array as $key => $value) {
            $query = "DELETE FROM products WHERE sku='$value'";
            $this->connection()->exec($query);
        }
    }
}   

?>