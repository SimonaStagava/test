<?php

class Dvd extends Product 
{
    public $firstTwo = 'DV';
    public $category = self::CATEGORIES[0];
    public $information = 'Please provide dimension in MB format';
    public $format = 'Size';
}

?>