<?php

class Db 
{
    private $dsn;
    private $username;
    private $password;

    protected function connection()
    {
        $this->dsn = "mysql:host=localhost;dbname=juniortest;";
        $this->username = "root";
        $this->password = "";

        $conn = new PDO($this->dsn, $this->username, $this->password);

        try {
            $conn = new PDO($this->dsn, $this->username, $this->password);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $db = "CREATE TABLE IF NOT EXISTS products(
                    sku char(7) UNIQUE NOT NULL,
                    name varchar(100) NOT NULL,
                    price float NOT NULL,
                    category varchar(20) NOT NULL,
                    size float NULL,
                    weight float NULL,
                    height float NULL,
                    width float NULL,
                    lenght float NULL);";
            $conn->exec($db);
            return $conn;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
}

?>