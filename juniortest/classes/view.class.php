<?php

class View extends Db
{
    public $sql;

    public function getAll()
    {
        $return = $this->connection()->query($this->sql);
        $return->execute();
        $name = $return->fetchAll();
        return $name;
    }
}

?>