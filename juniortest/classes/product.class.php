<?php 

class Product
{
    public $sku;
    public $firstTwo;
    public $itemIds = [];
    public $name;
    public $price;
    public const CATEGORIES = ['DVD', 'Book', 'Furniture'];
    public $information;
    public $format;

    public function randomSku() 
    {
        $array = str_split('0123456789');
        shuffle($array);
        $array = array_slice($array, 0, 5);
        $string = implode('', $array);
        $this->sku = $this->firstTwo . $string;
        if (empty($this->itemIds)) {
            $this->itemIds[] = $this->sku;
        } else {
            foreach ($this->itemIds as $itemId) {
                if ($itemId != $this->sku) {
                    $this->itemIds[] = $this->sku;
                } else {
                    randomSku();
                }
            }
        }
        echo $this->sku;
    }

    public function type() 
    {
        echo $this->category;
    }

    public function dropdownList()
    {
        $category = self::CATEGORIES;
        foreach ($category as $value) {
            $lower = strtolower($value);
            echo "<a class=\"dropdown-item\" href=\"productadd.php?page={$lower}\">{$value}</a>";
        }
    }

    public function specialAttribute() 
    {    
        echo "<h3>{$this->information}</h3>";
        echo '<div class="form-group">';
        
        $array = (is_array($this->format)) ? "True" : "False";
        if ($array == "True") {
            foreach ($this->format as $values) {
                echo "<label>{$values}</label>";
                $lower = strtolower($values);
                echo "<input type=\"text\" class=\"form-control\" name=\"{$lower}\" value=\"\">";
            }
        } else {
            echo "<label>{$this->format}</label>";
            $lower = strtolower($this->format);
            echo "<input type=\"text\" class=\"form-control\" name=\"{$lower}\" value=\"\">";
        }
        
        echo '</div>';
    }
}

?>