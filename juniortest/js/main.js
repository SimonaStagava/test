$(document).ready(function() {
    
    $('.checkAll').click(function() {
        if (this.checked) {
            $('.viewCheckbox').each(function() {
                this.checked = true;
            });
        } else {
            $('.viewCheckbox').each(function() {
                this.checked = false;
            });
        }
    });

    $('.delete').click(function() {
        var array = new Array();

        if ($('input:checkbox:checked').length > 0) {
            $('input:checkbox:checked').each(function() {
                array.push($(this).attr('id'));
                deleteData(array);
                $(this).closest('.view').remove();
            });
        }
    });

    function deleteData(array) {
        $.ajax({
            type: 'post',
            url: "productlist.php",
            data: {'data' : array},
        });
    }

});