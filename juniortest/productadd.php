<?php 
require_once './php/functions.php'; 
$v = new Validation($_POST);
if (isset($_POST['save']) && !$v->validation()) {
    $c = new Catalogue;
    $c->createDb();
    $c->insert();
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Product Add</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" media="screen" href="css/style.css" />
<style>

</style>
</head>
<body>
    <div class="container">
        <div class="header">
            <h2>Product add / <a href="productlist.php">Product list</a></h2>
        </div>
        <div class="form">
            <form method="post" action="<?= $_SERVER['PHP_SELF']; ?>">
                <div class="form-group">
                    <label>SKU
                    <?php if (isset($_POST['save']) && in_array('sku', $v->validation())) : ?>
                    <span>Requiered product identification number</span>
                    <?php endif; ?>
                    </label>
                    <input type="text" class="form-control" name="sku" value="<?php 
                    foreach ($_GET as $value) {
                        $sku = new $value;
                        $sku->randomSku();
                    }
                    if ($v->validation()) {
                        echo htmlentities($_POST['sku']);
                    }
                    ?>
                    ">
                </div>
                <div class="form-group">
                    <label>Name
                    <?php if (isset($_POST['save']) && in_array('name', $v->validation())) : ?>
                    <span class="span">Please enter product name</span>
                    <?php endif; ?>
                    </label>
                    <input type="text" class="form-control" name="name" 
                    <?php if ($v->validation()) {
                        echo 'value="' . htmlentities($_POST['name']) . '"';
                    }
                    ?>
                    >
                </div>
                <div class="form-group">
                    <label>Price
                    <?php if (isset($_POST['save']) && in_array('price', $v->validation())) : ?>
                    <span>Please enter product price</span>
                    <?php endif; ?>
                    </label>
                    <input type="text" class="form-control" name="price"
                    <?php if ($v->validation()) {
                        echo 'value="' . htmlentities($_POST['price']) .'"';
                    }
                    ?>
                    >
                </div>
                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Type switcher</button>
                    <?php if (isset($_POST['save']) && in_array('category', $v->validation())) : ?>
                    <span>Please choose product type</span>
                    <?php endif; ?>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" name="category">
                        <?php
                        $p = new Product;
                        $p->dropdownList();
                        ?>
                    </div>
                    <input type="text" class="form-control" name="category" value="<?php
                    foreach ($_GET as $value) {
                        $type = new $value;
                        $type->type();
                    }
                    if ($v->validation()) {
                        echo htmlentities($_POST['category']);
                    }
                    ?>" style="margin-top: 15px;">
                </div>
                <div class="specialAttribute">
                <?php 
                foreach ($_GET as $value) {
                    $attr = new $value;
                    $attr->specialAttribute();
                }
                if ($v->validation() && !in_array('category', $v->validation())) { 
                    $array = array_values($_POST);
                    $value = $array[3];  
                    $class = new $value;
                    $attr = $class->specialAttribute(); 
                    echo htmlentities($attr);            
                }
                if (isset($_POST['save'])) {
                    if (in_array('size', $v->validation()) || in_array('weight', $v->validation())) {
                        echo '<span>Please enter product format</span>';
                    } elseif (in_array('height', $v->validation()) || in_array('width', $v->validation()) || in_array('lenght', $v->validation())) {
                        echo '<span>Please enter all product formats</span>';
                    }
                }
                ?>   
                </div>
                <button type="submit" class="btn btn-secondary save" name="save">Save</button>
            </form>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>