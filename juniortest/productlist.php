<?php
require_once './php/functions.php'; 
$b = new BookView;
$books = $b->getAll();
$d = new DvdView;
$dvds = $d->getAll();
$f = new FurnitureView;
$furnitures = $f->getAll();
$c = new Catalogue;
if (isset($_POST['data'])) {
    $c->delete();
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Product List</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" media="screen" href="css/style.css" />
</head>
<body>
    <div class="container">
        <div class="header">
            <h2>Product list / <a href="productadd.php">Product add</a></h2>
            <div class="checkAllCheckboxes">
                <label>Select all</label>
                <input class="checkAll" type="checkbox">
            </div>
            <button class="btn btn-secondary delete" type="submit" name="delete">DELETE</button>
        </div>
        <div>
            <div>
                <h3>Books</h3>
            </div>
            <div class="catalogue">
                <?php foreach ($books as $book) : ?>
                <div class="view">
                    <div class="check">
                        <input id="<?php echo $book['sku']; ?>" class="viewCheckbox" type="checkbox">
                    </div>
                    <p><?php echo $book['sku']; ?></p>
                    <p><?php echo $book['name']; ?></p>
                    <p><?php echo $book['price']; ?> &euro;</p>
                    <p><?php echo $book['weight'] . ' KG'; ?></p>
                </div>
                <?php endforeach; ?>
            </div>
            <div>
                <h3>DVD's</h3>
            </div>
            <div class="catalogue">
                <?php foreach ($dvds as $dvd) : ?>
                <div class="view">
                    <div class="check">
                        <input id="<?php echo $dvd['sku']; ?>" class="viewCheckbox" type="checkbox">
                    </div>
                    <p><?php echo $dvd['sku']; ?></p>
                    <p><?php echo $dvd['name']; ?></p>
                    <p><?php echo $dvd['price']; ?> &euro;</p>
                    <p><?php echo $dvd['size'] . ' MB'; ?></p>
                </div>
                <?php endforeach; ?>
            </div>
            <div>
                <h3>Furniture</h3>
            </div>
            <div class="catalogue">
                <?php foreach ($furnitures as $furniture) : ?>
                <div class="view">
                    <div class="check">
                        <input id="<?php echo $furniture['sku']; ?>" class="viewCheckbox" type="checkbox">
                    </div>
                    <p><?php echo $furniture['sku']; ?></p>
                    <p><?php echo $furniture['name']; ?></p>
                    <p><?php echo $furniture['price']; ?> &euro;</p>
                    <p><?php echo $furniture['height'] . 'x' . $furniture['width'] . 'x' . $furniture['lenght'] . ' CM'; ?></p>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="js/main.js"></script>
</body>
</html>